/*
 * Copyright 2018 awz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.at.oven.service;

import com.at.oven.domain.OvenCycle;
import com.at.oven.domain.OvenState;
import com.at.oven.repository.OvenCycleRepository;
import com.at.oven.repository.OvenStateRepository;
import java.util.concurrent.TimeUnit;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Assert;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestConfig.class, initializers = ConfigFileApplicationContextInitializer.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class OvenServiceTest {
  
  @Autowired
  OvenCycleRepository ovenCycleRepository;
  
  @Autowired
  OvenStateRepository ovenStateRepository;
  
  @Autowired
  protected OvenCycleService ovenCycleService;
  
  @Autowired
  protected OvenStateService ovenStateService;
  
  @Autowired
  protected TimerService timerService;
  
  @Autowired
  private TestRestTemplate restTemplate;
  private RestTemplate restTemplateForPatch;
  
  @Before
  public void setup() {
    ovenCycleRepository.deleteAll();
    ovenStateRepository.deleteAll();
    
    this.restTemplateForPatch = restTemplate.getRestTemplate();
    HttpClient httpClient = HttpClientBuilder.create().build();
    this.restTemplateForPatch.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
    
    OvenCycle ovenCycle = new OvenCycle(1, "cycledescription1", "temperaturevalues1");
    ovenCycleRepository.save(ovenCycle);
    
    ovenCycle = new OvenCycle(2, "cycledescription2", "temperaturevalues2");
    ovenCycleRepository.save(ovenCycle);
    
    OvenState ovenState = new OvenState(1, 30);
    ovenStateRepository.save(ovenState);
  }
  
  @Test
  public void checkOvenCycleIfItExists() {
    assertThat(ovenCycleRepository.findById(1l)).isNotEqualTo(null);
    ResponseEntity<String> responseEntity = restTemplate.exchange("/cycle/1", HttpMethod.GET, new HttpEntity<>(""), String.class);
    assertThat(responseEntity.getStatusCode().value()).isEqualTo(HttpStatus.OK.value());
    assertThat(responseEntity.getBody()).isEqualTo("{\"id\":1,\"cycledescription\":\"cycledescription1\",\"temperaturevalues\":\"temperaturevalues1\"}");
    
    assertThat(ovenCycleRepository.findById(2l)).isNotEqualTo(null);
    responseEntity = restTemplate.exchange("/cycle/2", HttpMethod.GET, new HttpEntity<>(""), String.class);
    assertThat(responseEntity.getStatusCode().value()).isEqualTo(HttpStatus.OK.value());
    assertThat(responseEntity.getBody()).isEqualTo("{\"id\":2,\"cycledescription\":\"cycledescription2\",\"temperaturevalues\":\"temperaturevalues2\"}");
  }
  
  @Test
  public void checkOvenStateIfItExists() {
    assertThat(ovenStateRepository.findById(1l)).isNotEqualTo(null);
    ResponseEntity<String> responseEntity = restTemplate.exchange("/state", HttpMethod.GET, new HttpEntity<>(""), String.class);
    assertThat(responseEntity.getStatusCode().value()).isEqualTo(HttpStatus.OK.value());
    assertThat(responseEntity.getBody()).isEqualTo("{\"ovencycleid\":1,\"duration\":30}");
  }
  
  @Test
  public void checkDeleteOvenStateIfItExists() {
    assertThat(ovenStateRepository.findById(1l)).isNotEqualTo(null);
    ResponseEntity<String> responseEntity = restTemplate.exchange("/cycle/1", HttpMethod.DELETE, new HttpEntity<>(""), String.class);
    assertThat(responseEntity.getStatusCode().value()).isEqualTo(HttpStatus.OK.value());
    assertThat(responseEntity.getBody()).isEqualTo("{\"result\":\"Cycle was deleted\"}");
  }
  
  @Test
  public void checkDeleteOvenStateIfItNotExists() {
    assertThat(ovenStateRepository.findById(1l)).isNotEqualTo(null);
    ResponseEntity<String> responseEntity = restTemplate.exchange("/cycle/11", HttpMethod.DELETE, new HttpEntity<>(""), String.class);
    assertThat(responseEntity.getStatusCode().value()).isEqualTo(500);
    assertThat(responseEntity.getBody()).contains("\"errorMessage\":\"Cycle not found\",\"errorDetails\":\"uri=/cycle/11\"}");
  }
  
  @Test
  public void checkUpdateOvenCycleIfItExists() {
    String bodyText = "{\"id\":1,\"cycledescription\":\"cycledescription2\",\"temperaturevalues\":\"temperaturevalues2\"}";
    HttpHeaders headers = new HttpHeaders();
    headers.set("Content-Type", "application/json");
    HttpEntity<String> entity = new HttpEntity<>(bodyText, headers);
    ResponseEntity<String> exchange = restTemplateForPatch.exchange("/cycle", HttpMethod.POST, entity, String.class);
    assertThat(exchange.getStatusCode().value()).isEqualTo(HttpStatus.OK.value());
    assertThat(exchange.getBody()).isEqualTo("{\"result\":\"Cycle was updated\"}");
  }
  
  @Test
  public void checkUpdateOvenCycleIfItNotExists() {
    String bodyText = "{\"id\":11,\"cycledescription\":\"cycledescription11\",\"temperaturevalues\":\"temperaturevalues11\"}";
    HttpHeaders headers = new HttpHeaders();
    headers.set("Content-Type", "application/json");
    HttpEntity<String> entity = new HttpEntity<>(bodyText, headers);
    ResponseEntity<String> exchange = restTemplateForPatch.exchange("/cycle", HttpMethod.POST, entity, String.class);
    assertThat(exchange.getStatusCode().value()).isEqualTo(HttpStatus.OK.value());
    assertThat(exchange.getBody()).isEqualTo("{\"result\":\"Cycle was updated\"}");
  }  
  
  @Test
  public void checkStartAndStop() throws InterruptedException {    
    timerService.currentDuration.set(0);
    timerService.startTimer(30);
    TimeUnit.MILLISECONDS.sleep(1000);  
    timerService.stopTimer();       
    //Not implemented more, sorry...
  }

  
}
