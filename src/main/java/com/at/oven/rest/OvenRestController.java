/*
 * Copyright 2018 awz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.at.oven.rest;

import com.at.oven.domain.OvenCycle;
import com.at.oven.domain.OvenState;
import com.at.oven.exception.CycleNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.at.oven.service.OvenCycleService;
import com.at.oven.service.OvenStateService;
import com.at.oven.service.TimerService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class OvenRestController {

  private static final long STATE_ID = 1;

  @Autowired
  private OvenCycleService ovenCycleService;

  @Autowired
  private OvenStateService ovenStateService;

  @Autowired
  private TimerService timerService;

  @RequestMapping(value = "/cycle", method = RequestMethod.POST)
  public ResponseEntity<ProcessResult> updateCycle(@RequestBody OvenCycle ovenCycle) {
    ovenCycleService.save(ovenCycle);
    cycleIdCheck(ovenCycle.getId());
    return new ResponseEntity<>(new ProcessResult("Cycle was updated"), HttpStatus.OK);
  }

  @RequestMapping(value = "/cycle/{cycleId}", method = RequestMethod.GET)
  public ResponseEntity<OvenCycle> getCycle(@PathVariable Long cycleId) {
    Optional<OvenCycle> ovenCycle = ovenCycleService.findById(cycleId);
    cycleIdCheck(cycleId);
    return new ResponseEntity<>(ovenCycle.get(), HttpStatus.OK);
  }

  @RequestMapping(value = "/cycle/all", method = RequestMethod.GET)
  public ResponseEntity<List<OvenCycle>> getCycle() {
    Iterable<OvenCycle> ovenCycle = ovenCycleService.findAll();
    List<OvenCycle> ovenCycles = new ArrayList();
    ovenCycle.forEach(ovenCycles::add);
    if (ovenCycles.isEmpty()) {
      throw new CycleNotFoundException("Cycle list is empty");
    }
    return new ResponseEntity<>(ovenCycles, HttpStatus.OK);
  }

  @RequestMapping(value = "/cycle/{cycleId}", method = RequestMethod.DELETE)
  public ResponseEntity<ProcessResult> deleteCycle(@PathVariable Long cycleId) {
    cycleIdCheck(cycleId);
    ovenCycleService.deleteById(cycleId);
    if (ovenCycleService.findById(cycleId).isPresent()) {
      throw new CycleNotFoundException("Delete error");
    }
    return new ResponseEntity<>(new ProcessResult("Cycle was deleted"), HttpStatus.OK);
  }

  @RequestMapping(value = "/cycle/all", method = RequestMethod.DELETE)
  public ResponseEntity<ProcessResult> deleteCycle() {
    ovenCycleService.deleteAll();
    if (ovenCycleService.count() > 0) {
      throw new CycleNotFoundException("Delete error");
    }
    return new ResponseEntity<>(new ProcessResult("All cycles was deleted"), HttpStatus.OK);
  }

  @RequestMapping(value = "/state", method = RequestMethod.POST)
  public ResponseEntity<ProcessResult> setState(@RequestBody OvenState ovenState) {
    ovenStateService.save(ovenState);
    if (!ovenStateService.findById(STATE_ID).isPresent()) {
      throw new CycleNotFoundException("State setup error");
    }
    return new ResponseEntity<>(new ProcessResult("State was updated"), HttpStatus.OK);
  }

  @RequestMapping(value = "/state", method = RequestMethod.GET)
  public ResponseEntity<OvenState> getState() {
    Optional<OvenState> ovenState = ovenStateService.findById(STATE_ID);
    OvenState ovenStateRes = new OvenState(0l, 0);
    if (!ovenState.isPresent()) {
      ovenStateService.save(ovenStateRes);
    } else {
      ovenStateRes = ovenState.get();
    }
    return new ResponseEntity<>(ovenStateRes, HttpStatus.OK);
  }

  @RequestMapping(value = "/start", method = RequestMethod.POST)
  public ResponseEntity<ProcessResult> startProcess() {
    Optional<OvenState> ovenState = ovenStateService.findById(STATE_ID);
    if (!ovenState.isPresent()) {
      throw new CycleNotFoundException("Start error");
    }
    if (!timerService.sheduleTimer(ovenState.get().getDuration())) {
      throw new CycleNotFoundException("Another process");
    }
    return new ResponseEntity<>(new ProcessResult("Started"), HttpStatus.OK);
  }

  @RequestMapping(value = "/stop", method = RequestMethod.POST)
  public ResponseEntity<ProcessResult> stopProcess() {
    timerService.stopTimer();
    return new ResponseEntity<>(new ProcessResult("Stopped"), HttpStatus.OK);
  }

  private void cycleIdCheck(Long cycleId) {
    if (!ovenCycleService.findById(cycleId).isPresent()) {
      throw new CycleNotFoundException("Cycle not found");
    }
  }

}
