/*
 * Copyright 2018 awz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.at.oven.service.impl;

import com.at.oven.domain.OvenState;
import com.at.oven.repository.OvenStateRepository;
import com.at.oven.service.OvenStateService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OvenStateServiceImpl implements OvenStateService {

  @Autowired
  private OvenStateRepository ovenStateRepository;

  @Override
  public OvenState save(OvenState ovenState) {
    return ovenStateRepository.save(ovenState);
  }

  @Override
  public void deleteById(Long ovenStateId) {
    ovenStateRepository.deleteById(ovenStateId);
  }

  @Override
  public void deleteAll() {
    ovenStateRepository.deleteAll();
  }

  @Override
  public Optional<OvenState> findById(Long ovenStateId) {
    return ovenStateRepository.findById(ovenStateId);
  }

  @Override
  public Iterable<OvenState> findAll() {
    return ovenStateRepository.findAll();
  }

  @Override
  public long count() {
    return ovenStateRepository.count();
  }

}
