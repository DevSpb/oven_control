/*
 * Copyright 2018 awz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.at.oven.service.impl;

import com.at.oven.domain.OvenCycle;
import org.springframework.beans.factory.annotation.Autowired;
import com.at.oven.repository.OvenCycleRepository;
import com.at.oven.service.OvenCycleService;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class OvenCycleServiceImpl implements OvenCycleService {

  @Autowired
  private OvenCycleRepository ovenCycleRepository;

  @Override
  public OvenCycle save(OvenCycle ovenCycle) {
    return ovenCycleRepository.save(ovenCycle);
  }
  
  @Override
  public Iterable<OvenCycle> findAll() {
    return ovenCycleRepository.findAll();    
  }

  @Override
  public void deleteById(Long ovenCycleId) {
    ovenCycleRepository.deleteById(ovenCycleId);
  }

  @Override
  public void deleteAll() {
    ovenCycleRepository.deleteAll();
  }

  @Override
  public Optional<OvenCycle> findById(Long ovenCycleId) {
    return ovenCycleRepository.findById(ovenCycleId);
  }
  
  @Override
  public long count() {
    return ovenCycleRepository.count();
  }

}
