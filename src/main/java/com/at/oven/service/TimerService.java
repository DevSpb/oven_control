/*
 * Copyright 2018 awz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.at.oven.service;

import com.at.oven.domain.OvenState;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TimerService {

  @Autowired
  private OvenStateService ovenStateService;

  public AtomicLong currentDuration = new AtomicLong(0);
  private final int tickValue = 1000;
  static volatile ScheduledExecutorService service;
  static volatile Timer timer;

  public boolean sheduleTimer(long duration) {
    if (currentDuration.get() <= 0) {
      if ((duration > 0)) {
        currentDuration.set(duration);
        service = Executors.newSingleThreadScheduledExecutor();
        service.schedule(() -> {
          startTimer(duration);
        }, 10, TimeUnit.MILLISECONDS);
      }
    } else {
      return false;
    }
    return true;
  }

  public void startTimer(long duration) {
    TimerTask task = new TimerTask() {
      @Override
      public void run() {
        currentDuration.getAndDecrement();
        OvenState ovenState = ovenStateService.findById(1l).get();
        ovenStateService.save(new OvenState(ovenState.getOvencycleid(), currentDuration.get()));
        if (currentDuration.get() <= 0) {
          this.cancel();
          ovenStateService.save(new OvenState(0, 0));
        }
      }
    };
    timer = new Timer();
    timer.scheduleAtFixedRate(task, duration, tickValue);
  }

  public void stopTimer() {
    if (!service.isShutdown()) {
      timer.cancel();
      service.shutdownNow();
    }
    ovenStateService.save(new OvenState(0, 0));
    currentDuration.set(0);
  }

}
