/*
 * Copyright 2018 awz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.at.oven.exception;

import java.util.Date;

public class ExceptionResponse {

  private final Date errorDate;
  private final String errorMessage;
  private final String errorDetails;

  public ExceptionResponse(Date errorDate, String errorMessage, String errorDetails) {
    super();
    this.errorDate = errorDate;
    this.errorMessage = errorMessage;
    this.errorDetails = errorDetails;
  }

  public Date getErrorDate() {
    return errorDate;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public String getErrorDetails() {
    return errorDetails;
  }

}
