/*
 * Copyright 2018 awz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.at.oven.domain;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "ovencycle")
public class OvenCycle implements Serializable {

  @Id
  @Column(nullable = false)  
  private long id;

  @Column(nullable = false)
  private String cycledescription;

  @Column(nullable = false)
  private String temperaturevalues;

  protected OvenCycle() {
  }

  public OvenCycle(String cycledescription, String temperaturevalues) {
    this.cycledescription = cycledescription;
    this.temperaturevalues = temperaturevalues;
  }
  
  public OvenCycle(long id, String cycledescription, String temperaturevalues) {
    this.id = id;
    this.cycledescription = cycledescription;
    this.temperaturevalues = temperaturevalues;
  }  

  public long getId() {
    return id;
  }

  public String getCycledescription() {
    return cycledescription;
  }

  public String getTemperaturevalues() {
    return temperaturevalues;
  }

}
