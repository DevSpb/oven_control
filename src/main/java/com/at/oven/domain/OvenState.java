/*
 * Copyright 2018 awz.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.at.oven.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

@Entity
@JsonIgnoreProperties(value = {"id"})
@Table(name = "ovenstate")
public class OvenState implements Serializable {

  @Id
  private final long id = 1l;

  @Column(nullable = false)
  private long ovencycleid;

  @Column(nullable = false)
  private long duration;

  protected OvenState() {
  }

  public OvenState(long ovencycleid, long duration) {
    this.ovencycleid = ovencycleid;
    this.duration = duration;
  }

  public long getOvencycleid() {
    return ovencycleid;
  }

  public long getDuration() {
    return duration;
  }

}
