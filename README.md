#Backend developer test
###Appliance Control

The task is to design and implement a backend service to control an appliance such as a wash machine or an oven. The API should be REST
based and the state of the appliance should be persisted to any form of persistent storage. There is no need for any frontend but we expect there
to be a README.md file with build directions and examples of how to invoke the REST API (e.g. curl).

The project should be implemented using Java or Node.js. Feel free to use any 3rd party library that you are comfortable with. Unit tests are
expected and the assignment will be assessed based on good programming practices and design.

Please use GitHub or Bitbucket to publish your source code.

###Installation and Run

`$ mvn spring-boot:run`


How to build and run
--------------------
1. Open any command line terminal.

2. Go to the root project folder and run:

   mvn clean install

   There will be an executable JAR file.

3. Run the JAR:

   java -jar target\oven-control-service-0.0.1-SNAPSHOT.jar
   
   You can see in log when the service is loaded.

REST API
--------
Implemented API is very simple and clear.

Response is sent in JSON format. 
   
This is an example of API invocation using curl:

    curl http://localhost:8090/cycle
    
    curl http://localhost:8090/cycle/all 
    
    curl http://localhost:8090/state
    
    curl http://localhost:8090/start  
    
    curl http://localhost:8090/stop    
    

DBMS
----
In this project H2 in-memory database is used. 
